# Template me

## Helper to run content through templating

- Download latest index.js and/or prep.js
- TO Run go to CMD (Type CMD in Crumb bar)
- type:
  
```node index.js [data] [output]```

`[data]` is optional: use this to change the input folder (if not added the default is "data")

[output] is optional: use this to change the output folder (if not added the default is "data")

The file template.html inside the [data] fodler is used and merged against all files with .json as extension creating as many .html files in the folder [output]

=====================================================

To be able to prep data files:

```node prep.js [en] [es] [fr]```

All variables in the file: "data/template.html" will be extracted in JSON format and files with as many commandline arguments as passed will be created in the data folder with the .json extension
To change inout and output directories just add the named arguments at the end, example:

```node prep.js en es de --input=data2 --output=myoutput```



# Steps to follow:

- Create template
- Place template in folder called `data` with name `template.html`
- load CMD
- run prep to create json files structures ready:
    - ```node prep.js [en] [es]```
- fill in the empty variable in the json files
- run index generate html from template merged with json files
    - ```node index.js```
