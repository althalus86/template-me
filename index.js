const express = require('express');
const app = express();
const Handlebars = require("handlebars");
const async = require("async");
const fs = require('fs');
const port = 3000
var directoryPath = "data";
var outputDirectoryPath = "data";

// overloading commandline parameters
//var myArgs = process.argv.slice(2);

var argv = require('minimist')(process.argv.slice(2));
var myArgs = argv._;
if (argv.input) directoryPath = argv.input;
if (argv.output) outputDirectoryPath = argv.output;
if (myArgs[0]) directoryPath = myArgs[0];
if (myArgs[1]) outputDirectoryPath = myArgs[1];

async.parallel([
	function (callback) {
		fs.readFile(`./${directoryPath}/template.html`, 'utf8', function (err, data) {
			if (err) {
				return console.log(err);
			}
			callback(null, data);
		});
	},
	function (callback) {
		var jsonFiles = [];
		fs.readdir(directoryPath, function (err, files) {
			//handling error
			if (err) {
				return console.log('Unable to scan directory: ' + err);
			}
			//listing all files using forEach
			files.forEach(function (file) {
				// Do whatever you want to do with the file
				if (!file.endsWith("json")) return;
				jsonFiles.push({ name: file, data: JSON.parse(fs.readFileSync(`${directoryPath}/${file}`)) })
			});
			callback(null, jsonFiles);
		});
	}
],
	function (err, results) {
		var template = Handlebars.compile(results[0]);
		if (!fs.existsSync(outputDirectoryPath)) {
			fs.mkdirSync(outputDirectoryPath);
		}
		results[1].forEach(function (file) {
			var output = template(file.data);
			var newFile = file.name.slice(0, -5);
			fs.writeFile(`${outputDirectoryPath}/${newFile}.html`, output, function (err) {
				if (err) return console.log(err);
				console.log(`written: ${newFile}.html`);
			});
		})
	});
