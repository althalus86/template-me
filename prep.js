const express = require('express');
const app = express();
const Handlebars = require("handlebars");
const async = require("async");
const fs = require('fs');
const port = 3000
var directoryPath = "data";
var outputDirectoryPath = "data";

var argv = require('minimist')(process.argv.slice(2));
var myArgs = argv._;
if (argv.input) directoryPath = argv.input;
if (argv.output) outputDirectoryPath = argv.output;

fs.readFile(`./${directoryPath}/template.html`, 'utf8', function (err, data) {
	if (err) {
		return console.log(err);
	}
	var json = {}
	var vars = [];
	const regexp = /{{(.*)}}/g;
	var extract = [...data.match(regexp)]
	extract.forEach(function (item) {
		vars.push(item.substring(2, item.length - 2))
		json[item.substring(2, item.length - 2)] = "___Placeholder___"
	})
	console.log(json);
	if (!fs.existsSync(outputDirectoryPath)) {
		fs.mkdirSync(outputDirectoryPath);
	}
	myArgs.forEach(function (arg) {
		fs.writeFile(`${outputDirectoryPath}/${arg}.json`, JSON.stringify(json, null, 4), function (err) {
			if (err) return console.log(err);
			console.log("ready");
		})
	});
})
